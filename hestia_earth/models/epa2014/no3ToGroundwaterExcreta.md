## NO3, to groundwater, excreta

Nitrate leaching to groundwater, from animal excreta.

### Returns

* A list of [Emissions](https://hestia.earth/schema/Emission) with:
  - [term](https://hestia.earth/schema/Emission#term) with [no3ToGroundwaterExcreta](https://hestia.earth/term/no3ToGroundwaterExcreta)
  - [methodModel](https://hestia.earth/schema/Emission#methodModel) with [epa2014](https://hestia.earth/term/epa2014)
  - [value](https://hestia.earth/schema/Emission#value)
  - [methodTier](https://hestia.earth/schema/Emission#methodTier) with `tier 2`

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a [site](https://hestia.earth/schema/Cycle#site) with:
    - [siteType](https://hestia.earth/schema/Site#siteType) with `cropland` **or** `permanent pasture` **or** `animal housing` **or** `pond` **or** `river or stream` **or** `lake` **or** `sea or ocean`
  - a list of [inputs](https://hestia.earth/schema/Cycle#inputs) with:
    - [term](https://hestia.earth/schema/Input#term) of [termType](https://hestia.earth/schema/Term#termType) = [excreta](https://hestia.earth/glossary?termType=excreta)
  - a list of [practices](https://hestia.earth/schema/Cycle#practices) with:
    - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) of [termType](https://hestia.earth/schema/Term#termType) = [excretaManagement](https://hestia.earth/glossary?termType=excretaManagement)

This model works on the following Node type with identical requirements:

* [Cycle](https://hestia.earth/schema/Cycle)
* [Transformation](https://hestia.earth/schema/Transformation)

### Lookup used

- [excretaManagement.csv](https://hestia.earth/glossary/lookups/excretaManagement.csv) -> `EF_NO3-N`
- [emission.csv](https://hestia.earth/glossary/lookups/emission.csv) -> `siteTypesAllowed`; `inputTermTypesAllowed`; `typesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.epa2014 import run

print(run('no3ToGroundwaterExcreta', Cycle))
```
