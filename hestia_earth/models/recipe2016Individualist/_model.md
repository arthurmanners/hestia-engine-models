# ReCiPe 2016 Individualist

These models characterise emissions and resource uses according to the ReCiPe 2016 method, using an individualist perspective (see [Huijbregts et al (2016)](https://www.rivm.nl/bibliotheek/rapporten/2016-0104.pdf)).
