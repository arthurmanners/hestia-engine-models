## Carbon content

The total (organic plus mineral) carbon content of something, as C, expressed as a percentage.

### Returns

* A list of [Products](https://hestia.earth/schema/Product) with:
  - a list of [properties](https://hestia.earth/schema/Product#properties) with:
    - [term](https://hestia.earth/schema/Property#term) with [carbonContent](https://hestia.earth/term/carbonContent)
    - [methodModel](https://hestia.earth/schema/Property#methodModel) with [ipcc2019](https://hestia.earth/term/ipcc2019)
    - [value](https://hestia.earth/schema/Property#value)

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a list of [products](https://hestia.earth/schema/Cycle#products) with:
    - [term](https://hestia.earth/schema/Product#term) of [termType](https://hestia.earth/schema/Term#termType) = [crop](https://hestia.earth/glossary?termType=crop) **or** [forage](https://hestia.earth/glossary?termType=forage) and [value](https://hestia.earth/schema/Product#value) `> 0` and optional:
      - a list of [properties](https://hestia.earth/schema/Product#properties) with:
        - [value](https://hestia.earth/schema/Property#value) and [term](https://hestia.earth/schema/Property#term) with [dryMatter](https://hestia.earth/term/dryMatter)

### Lookup used

- [crop.csv](https://hestia.earth/glossary/lookups/crop.csv) -> `IPCC_2019_Ratio_AGRes_YieldDM`; `IPCC_2019_Ratio_BGRes_AGRes`; `C_CONTENT_AG_CROP_RESIDUE`; `C_CONTENT_BG_CROP_RESIDUE`
- [forage.csv](https://hestia.earth/glossary/lookups/forage.csv) -> `IPCC_2019_Ratio_AGRes_YieldDM`; `IPCC_2019_Ratio_BGRes_AGRes`; `C_CONTENT_AG_CROP_RESIDUE`; `C_CONTENT_BG_CROP_RESIDUE`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.ipcc2019 import run

print(run('carbonContent', Cycle))
```
