## Full Grass Consumption

This model estimates the energetic requirements of ruminants and can be used to estimate the amount of grass they graze.
Source:
[IPCC 2019, Vol.4, Chapter 10](https://www.ipcc-nggip.iges.or.jp/public/2019rf/pdf/4_Volume4/19R_V4_Ch10_Livestock.pdf).

### Returns

* A list of [Inputs](https://hestia.earth/schema/Input) with:
  - [methodModel](https://hestia.earth/schema/Input#methodModel) with [ipcc2019](https://hestia.earth/term/ipcc2019)
  - [term](https://hestia.earth/schema/Input#term) of [termType](https://hestia.earth/schema/Term#termType) = [crop](https://hestia.earth/glossary?termType=crop) **or** [forage](https://hestia.earth/glossary?termType=forage)
  - [value](https://hestia.earth/schema/Input#value)

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - Data completeness assessment for animalFeed: [completeness.animalFeed](https://hestia.earth/schema/Completeness#animalFeed) must be `False`
  - a [site](https://hestia.earth/schema/Cycle#site) with:
    - [siteType](https://hestia.earth/schema/Site#siteType) with `permanent pasture`
  - a list of [practices](https://hestia.earth/schema/Cycle#practices) with:
    - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) with [pastureGrass](https://hestia.earth/term/pastureGrass) and a [key](https://hestia.earth/schema/Practice#key) with:
      - [term](https://hestia.earth/schema/Term#term) of [termType](https://hestia.earth/schema/Term#termType) = [crop](https://hestia.earth/glossary?termType=crop) **or** [forage](https://hestia.earth/glossary?termType=forage)
  - a list of [animals](https://hestia.earth/schema/Cycle#animals) with:
    - [value](https://hestia.earth/schema/Animal#value) `> 0` and [term](https://hestia.earth/schema/Animal#term) of [termType](https://hestia.earth/schema/Term#termType) = [liveAnimal](https://hestia.earth/glossary?termType=liveAnimal) and [referencePeriod](https://hestia.earth/schema/Animal#referencePeriod) with `average` and a list of [properties](https://hestia.earth/schema/Animal#properties) with:
      - [value](https://hestia.earth/schema/Property#value) and [term](https://hestia.earth/schema/Property#term) with [liveweightPerHead](https://hestia.earth/term/liveweightPerHead) **or** [weightAtMaturity](https://hestia.earth/term/weightAtMaturity) and optional:
      - a list of [properties](https://hestia.earth/schema/Animal#properties) with:
        - [value](https://hestia.earth/schema/Property#value) and [term](https://hestia.earth/schema/Property#term) with [hoursWorkedPerDay](https://hestia.earth/term/hoursWorkedPerDay) **or** [pregnancyRateTotal](https://hestia.earth/term/pregnancyRateTotal) **or** [animalsPerBirth](https://hestia.earth/term/animalsPerBirth)
      - a list of [practices](https://hestia.earth/schema/Animal#practices) with:
        - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) of [termType](https://hestia.earth/schema/Term#termType) = [animalManagement](https://hestia.earth/glossary?termType=animalManagement) and a list of [properties](https://hestia.earth/schema/Practice#properties) with:
          - [value](https://hestia.earth/schema/Property#value) and [term](https://hestia.earth/schema/Property#term) with [fatContent](https://hestia.earth/term/fatContent)
  - optional:
    - a list of [products](https://hestia.earth/schema/Cycle#products) with:
      - [value](https://hestia.earth/schema/Product#value) and [term](https://hestia.earth/schema/Product#term) with [animalProduct](https://hestia.earth/term/animalProduct)

### Lookup used

- [animalManagement.csv](https://hestia.earth/glossary/lookups/animalManagement.csv) -> `mjKgEvMilkIpcc2019`
- [animalProduct.csv](https://hestia.earth/glossary/lookups/animalProduct.csv) -> `mjKgEVwoolNetEnergyWoolIpcc019`
- [liveAnimal.csv](https://hestia.earth/glossary/lookups/liveAnimal.csv) -> `ipcc2019AnimalTypeGrouping`; `mjDayKgCfiNetEnergyMaintenanceIpcc2019`; `ratioCPregnancyNetEnergyPregnancyIpcc2019`; `ratioCNetEnergyGrowthCattleBuffaloIpcc2019`; `MjKgABNetEnergyGrowthSheepGoatsIpcc2019`
- [system-liveAnimal-activityCoefficient-ipcc2019.csv](https://hestia.earth/glossary/lookups/system-liveAnimal-activityCoefficient-ipcc2019.csv) -> using animal term @id
- [crop-property.csv](https://hestia.earth/glossary/lookups/crop-property.csv) -> `energyDigestibilityRuminants`; `energyContentHigherHeatingValue`
- [crop.csv](https://hestia.earth/glossary/lookups/crop.csv) -> `grazedPastureGrassInputId`
- [forage-property.csv](https://hestia.earth/glossary/lookups/forage-property.csv) -> `energyDigestibilityRuminants`; `energyContentHigherHeatingValue`
- [forage.csv](https://hestia.earth/glossary/lookups/forage.csv) -> `grazedPastureGrassInputId`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.ipcc2019 import run

print(run('pastureGrass', Cycle))
```
