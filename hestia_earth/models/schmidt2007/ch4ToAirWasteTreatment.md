## CH4, to air, waste treatment

Methane emissions to air, from the treatment of wastes excluding excreta.

### Returns

* A list of [Emissions](https://hestia.earth/schema/Emission) with:
  - [term](https://hestia.earth/schema/Emission#term) with [ch4ToAirWasteTreatment](https://hestia.earth/term/ch4ToAirWasteTreatment)
  - [methodModel](https://hestia.earth/schema/Emission#methodModel) with [schmidt2007](https://hestia.earth/term/schmidt2007)
  - [value](https://hestia.earth/schema/Emission#value)
  - [methodTier](https://hestia.earth/schema/Emission#methodTier) with `tier 1`

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - either:
    - a list of [products](https://hestia.earth/schema/Cycle#product) with:
      - [value](https://hestia.earth/schema/Product#value) and [term](https://hestia.earth/schema/Product#term) of [termType](https://hestia.earth/schema/Term#termType) = [waste](https://hestia.earth/glossary?termType=waste)
    - Data completeness assessment for waste: [completeness.waste](https://hestia.earth/schema/Completeness#waste)

This model works on the following Node type with identical requirements:

* [Cycle](https://hestia.earth/schema/Cycle)
* [Transformation](https://hestia.earth/schema/Transformation)

### Lookup used

- [waste.csv](https://hestia.earth/glossary/lookups/waste.csv) -> `ch4EfSchmidt2007`
- [emission.csv](https://hestia.earth/glossary/lookups/emission.csv) -> `typesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.schmidt2007 import run

print(run('ch4ToAirWasteTreatment', Cycle))
```
