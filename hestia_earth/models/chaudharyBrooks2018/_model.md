# Chaudhary Brooks (2018)

This model calculates the biodiversity impacts related to habitat loss, accounting for different land use intensities, as defined in [Chaudhary & Brooks (2018)](https://doi.org/10.1021/acs.est.7b05570).
