## Cycle duration

This model calculates the cycle duration using the cropping intensity for a single year.

### Returns

- a `number` or `None` if requirements are not met

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - [cycleDuration](https://hestia.earth/schema/Cycle#cycleDuration) with `365`
  - [endDate](https://hestia.earth/schema/Cycle#endDate)
  - a list of [practices](https://hestia.earth/schema/Cycle#practices) with:
    - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) with [croppingIntensity](https://hestia.earth/term/croppingIntensity)
  - a [site](https://hestia.earth/schema/Cycle#site) with:
    - [siteType](https://hestia.earth/schema/Site#siteType) with `cropland`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.cycle import run

print(run('cycleDuration', Cycle))
```
