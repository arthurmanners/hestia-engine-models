## Practice Value

This model uses the lookup called "defaultValue" on each Practice to gap-fill a default value.
Otherwise, it calculates the `value` of the [Practice](https://hestia.earth/schema/Practice)
by taking an average from the `min` and `max` values.

### Returns

* A list of [Practices](https://hestia.earth/schema/Practice) with:
  - [value](https://hestia.earth/schema/Practice#value)

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a list of [practices](https://hestia.earth/schema/Cycle#practices) with:
    - [min](https://hestia.earth/schema/Practice#min) and [max](https://hestia.earth/schema/Practice#max)

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.cycle import run

print(run('practice.value', Cycle))
```
