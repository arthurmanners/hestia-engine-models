## Machinery infrastructure, depreciated amount per Cycle

The quantity of machinery infrastructure, depreciated over its lifetime, divided by the area it operates over, and expressed in kilograms per functional unit per Cycle.

Machinery gradually depreciates over multiple production Cycles until it reaches the
[end of its life](https://hestia.earth/schema/Infrastructure#endDate).
As a rough rule, the more the machinery is used, the faster it depreciates.
Machinery use can be proxied for by the amount of fuel used.
From 139 processes in [AGRIBALYSE](https://agribalyse.ademe.fr/), the ratio of machinery depreciated per unit of
fuel consumed (kg machinery kg diesel–1) was established.
Recognizing that farms in less developed countries have poorer access to capital and maintain farm machinery for longer,
the machinery-to-diesel ratio was doubled in countries with a [Human Development Index](http://hdr.undp.org/en/data)
of less than 0.8.

### Returns

* A list of [Inputs](https://hestia.earth/schema/Input) with:
  - [term](https://hestia.earth/schema/Input#term) with [machineryInfrastructureDepreciatedAmountPerCycle](https://hestia.earth/term/machineryInfrastructureDepreciatedAmountPerCycle)
  - [methodModel](https://hestia.earth/schema/Input#methodModel) with [agribalyse2016](https://hestia.earth/term/agribalyse2016)
  - [value](https://hestia.earth/schema/Input#value)

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a list of [inputs](https://hestia.earth/schema/Cycle#inputs) with:
    - [term](https://hestia.earth/schema/Input#term) of [termType](https://hestia.earth/schema/Term#termType) = [fuel](https://hestia.earth/glossary?termType=fuel) and [value](https://hestia.earth/schema/Input#value)
  - Data completeness assessment for material: [completeness.material](https://hestia.earth/schema/Completeness#material) must be `False`
  - a [site](https://hestia.earth/schema/Cycle#site) with:
    - a [country](https://hestia.earth/schema/Site#country) with:
      - [termType](https://hestia.earth/schema/Term#termType) = [region](https://hestia.earth/glossary?termType=region)

### Lookup used

- [region.csv](https://hestia.earth/glossary/lookups/region.csv) -> `HDI`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.agribalyse2016 import run

print(run('machineryInfrastructureDepreciatedAmountPerCycle', Cycle))
```
